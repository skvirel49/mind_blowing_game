package com.bilas.view;

import com.bilas.controller.Controller;
import com.bilas.controller.MyController;

import java.util.Scanner;

public final class MyView implements Printable {
    private Controller controller;
    private final Scanner scanner = new Scanner(System.in);
    public void print() {
        controller = new MyController();
        while (true) {
            outputMenu();
            System.out.println("please make your choice");
            switcher(scanner.nextInt());
        }
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        System.out.println("1 - Print score");
        System.out.println("2 - Roll drum");
        System.out.println("3 - Pull trigger");
        System.out.println("0 - to exit");
    }

    public void switcher(int num) {
        switch (num) {
            case 1:
                pressButton1();
                break;
            case 2:
                pressButton2();
                break;
            case 3:
                pressButton3();
                break;
            case 0:
                System.exit(0);
                break;
            default:
                System.out.println("Wrong key!");
                break;
        }
    }

    private void pressButton1() {
        System.out.print("Your score is: ");
        controller.showScore();
    }

    private void pressButton2() {
        controller.rollingDrum();
    }
    private void pressButton3() {
        controller.shoot();
    }
}
