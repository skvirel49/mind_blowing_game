package com.bilas.view;

/**
 * Interface has method print
 * @author Oleg Bilas
 */
public interface Printable {
    /**
     * Method used for view program
     * Shows menu and receive commands from user
     * Prints info
     */
    void print();
}
