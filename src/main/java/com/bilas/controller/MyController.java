package com.bilas.controller;

import com.bilas.model.Game;
import com.bilas.model.Model;

public final class MyController implements Controller {
    private Model model;

    public MyController() {
        this.model = new Game();
    }

    public void showScore() {
        model.showScore();
    }

    public void rollingDrum() {
        model.rollingDrum();
    }

    public void shoot() {
        model.shoot();
    }
}
