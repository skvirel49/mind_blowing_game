package com.bilas.controller;

public interface Controller {
    void showScore();
    void rollingDrum();
    void shoot();

}
