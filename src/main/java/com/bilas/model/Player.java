package com.bilas.model;

/**
 * Class Player.
 * @author Oleg Bilas
 */

public final class Player implements Shoot, RollingDrum {

    private String nickName;
    private Gun gun;
    private int score;

    public Player() {
        this.nickName = "player";
        this.gun = new Gun();
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(final String nickName) {
        this.nickName = nickName;
    }

    public Gun getGun() {
        return gun;
    }

    public void setGun(final Gun gun) {
        this.gun = gun;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Player{" +
                "nickName='" + nickName + '\'' +
                ", gun=" + gun +
                '}';
    }

    /**
     * Method rollingDrum set bullet to slot and roll drum
     */
    public void rollingDrum() {
        gun.setBulletInSlot();
        System.out.println("roll drum");
        gun.setSlotToBarrel((int) (Math.random() * gun.getDRUM_SIZE()));
    }

    /**
     * Method make pulling the trigger and print wos try lucky or not
     * Ant add point to score if player survive
     */
    public void shoot() {
        System.out.println("pull the trigger");
        if (gun.getDrumSlots()[gun.getSlotToBarrel()] == gun.getBULLET()) {
            System.out.println("SPLAT! " +
                    "brains were splattered all over the floor.");
        } else {
            System.out.println("WOW! You survive.");
            score++;
        }
    }
}
