package com.bilas.model;

public final class Game implements Model {

    private Player player;

    public Game() {
        this.player = new Player();
    }

    public void showScore() {
        System.out.println(player.getScore());
    }

    public void rollingDrum() {
        player.rollingDrum();
    }

    public void shoot() {
        player.shoot();
    }
}
