package com.bilas.model;

/**
 * Interface used to roll drum in gun.
 * @author Oleg Bilas
 */

public interface RollingDrum {
    /**
     * Method used to roll the drum.
     */
    void rollingDrum();
}
