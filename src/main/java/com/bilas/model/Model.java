package com.bilas.model;

/**
 * Interface extends two interfaces.
 * Has method
 */

public interface Model extends RollingDrum, Shoot {

    void showScore();

}
