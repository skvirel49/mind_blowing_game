package com.bilas.model;

import java.util.Arrays;

public class Gun {

    private final int DRUM_SIZE = 6;
    private final int BULLET = 1;
    private int[] drumSlots;
    private int slotToBarrel;

    public Gun() {
        this.drumSlots = new int[DRUM_SIZE];
    }

    public void setSlotToBarrel(final int slotToBarrel) {
        this.slotToBarrel = slotToBarrel;
    }

    public int getSlotToBarrel() {
        return slotToBarrel;
    }

    public int getDRUM_SIZE() {
        return DRUM_SIZE;
    }

    public int getBULLET() {
        return BULLET;
    }

    public int[] getDrumSlots() {
        return drumSlots;
    }

    public void setDrumSlots(int[] drumSlots) {
        this.drumSlots = drumSlots;
    }

    @Override
    public String toString() {
        return "Gun{" +
                "DRUM_SIZE=" + DRUM_SIZE +
                ", BULLET=" + BULLET +
                ", drumSlots=" + Arrays.toString(drumSlots) +
                '}';
    }

    public void setBulletInSlot() {
        drumSlots[(int) ((Math.random() * DRUM_SIZE))] = BULLET;
    }
}
