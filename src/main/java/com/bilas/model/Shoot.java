package com.bilas.model;

/**
 * Interface used to execute method shoot.
 * @author Oleg Bilas
 */
public interface Shoot {
    /**
     * Method pull the trigger and shows if shot was lucky or not.
     */
    void shoot();
}
