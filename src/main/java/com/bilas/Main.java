package com.bilas;

import com.bilas.view.MyView;

/**
 * This class execute program Russian Roulette.
 * Consist of method main.
 *
 * @author OlegBilas
 * @version 1.0
 * @since 7.09.2019
 */

public class Main {

    /**
     * Method <strong>main</strong> execute program.
     * @param args Parameters from Command Line.
     */
    public static void main(String[] args) {
        MyView myView = new MyView();
        myView.print();
    }
}
